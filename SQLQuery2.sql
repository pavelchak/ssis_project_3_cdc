
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[stg_Person_UPDATES]') AND type in (N'U'))
BEGIN
   SELECT TOP 0 * INTO stg_Person_UPDATES
   FROM Person
END
 
IF NOT EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[stg_Person_DELETES]') AND type in (N'U'))
BEGIN
   SELECT TOP 0 * INTO stg_Person_DELETES
   FROM Person
END


------------------------------------------------------------------------------------

-- batch update
UPDATE Person
SET
    Person.[PersonType] = stg.[PersonType],
    Person.[NameStyle] = stg.[NameStyle],
    Person.[Title] = stg.[Title],
    Person.[FirstName] = stg.[FirstName],
	Person.[MiddleName] = stg.[MiddleName],
	Person.[LastName] = stg.[LastName],
	Person.[Suffix] = stg.[Suffix],
	Person.[EmailPromotion] = stg.[EmailPromotion],
	Person.[rowguid] = stg.[rowguid],
	Person.[ModifiedDate] = stg.[ModifiedDate]
FROM
    Person JOIN stg_Person_UPDATES stg ON stg.BusinessEntityID = Person.BusinessEntityID
 

-- batch delete
DELETE FROM Person
WHERE BusinessEntityID IN
(SELECT BusinessEntityID FROM stg_Person_DELETES)

--------------------------------------------------------------------
Truncate table stg_Person_UPDATES;
Truncate table stg_Person_DELETES;





